from cerberus import Validator
from flask import request, current_app, json
from flask_api import status


APP_PACKAGE: str = "app"

METHOD_FIELD: str = "__method__"
VALIDATION_MODULE: str = "validators"
VALIDATION_OBJ_PREFIX: str = "validator"
STATIC_SEND_VIEW: str = "send_static_file"
EMPTY_VALIDATOR_ERR: str = "No keys/fields defined in the validator class."


def import_obj(module_path: str, obj_name: str):
    mod = __import__(module_path, fromlist=[obj_name])
    return getattr(mod, obj_name)
    

def validation_callback():
    try:
        if request.url_rule is None:
            return current_app.response_class(
                response=json.dumps({"method": request.method, "route": request.path}),
                status=status.HTTP_404_NOT_FOUND,
                mimetype='application/json'
            )

        req = request.get_json(force=True, silent=True)
        if req is None:
            req = {}
        req[METHOD_FIELD] = request.method
        view_func_name: str = current_app.view_functions.get(request.url_rule.endpoint, None).__name__
        if view_func_name == STATIC_SEND_VIEW:
            return None
        module_path: str = f"{APP_PACKAGE if request.blueprint is None else request.blueprint}.{VALIDATION_MODULE}"
        obj_name: str = f"{VALIDATION_OBJ_PREFIX}_{view_func_name}"
        validator: dict = import_obj(module_path, obj_name)
        success: bool = validator(req)
        if success:
            return None
        else:
            return current_app.response_class(
                response=json.dumps(validator.errors),
                status=status.HTTP_400_BAD_REQUEST,
                mimetype='application/json'
            )
    except Exception as e:
        if str(e) == EMPTY_VALIDATOR_ERR:
            return None
        else:    
            return current_app.response_class(
                    response=json.dumps(repr(e)),
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                    mimetype='application/json'
            )
