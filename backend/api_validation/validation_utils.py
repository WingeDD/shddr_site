from api_validation.validation_callback import METHOD_FIELD


def add_http_method_validation(allowed_methods: list[str], schema: dict) -> dict:
    schema[METHOD_FIELD] = {
        'required': True,
        'type': 'string',
        'allowed': allowed_methods
    }
    return schema
