## Deployment

`python -m venv venv`  
`source venv/bin/activate`  
`pip3 install -r requirements.txt`  
`flask db init`  
`flask db migrate`  
`flask db upgrade`  
`flask run`  
*`python3 -m pip install --upgrade 'sqlalchemy<2.0'`*  
