from werkzeug.datastructures import Headers
from flask import Blueprint, current_app, json, url_for, request
from flask_login import LoginManager
from flask_api import status
from auth.login_user import LoginUser
from api_validation.validation_callback import validation_callback


LOGIN_VIEW: str = "auth.login"


auth: Blueprint = Blueprint('auth', __name__, static_folder='/', template_folder='templates')
@auth.before_request
def wrapper():
    return validation_callback()

login_manager: LoginManager = LoginManager()

@login_manager.user_loader
def load_user(user_id: str) -> LoginUser:
    try:
        return LoginUser(user_id)
    except Exception:
        return None

@login_manager.unauthorized_handler
def unauthorized():
    data = {"redirection_route": url_for(LOGIN_VIEW)}
    if request.mimetype == "text/html" and request.method == 'GET':
        data["forwarded_route"] = request.path
    return current_app.response_class(
        response=json.dumps(data),
        status=status.HTTP_401_UNAUTHORIZED,
        mimetype='application/json',
        # headers=Headers({"Location": url_for(LOGIN_VIEW)})
    )


from auth import routes
