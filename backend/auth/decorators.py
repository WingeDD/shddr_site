from typing import Iterable
from functools import wraps
from werkzeug.datastructures import Headers
from flask import current_app, json, url_for
from flask_login import current_user
from flask_api import status
from db_handle.enums import UserStatus


def is_verified(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if current_user.is_verified == False:
            return current_app.response_class(
                response=json.dumps({"redirection_route": url_for('auth.verify')}),
                status=status.HTTP_302_FOUND,
                mimetype='application/json',
                # headers=Headers({"Location": url_for('auth.verify')})
            )
        return func(*args, **kwargs)
    return decorated_function

def status_required(available_for: Iterable[UserStatus]):
    def actual_decorator(func):
        @wraps(func)
        def decorated_function(*args, **kwargs):
            stat: UserStatus = current_user.get_status()
            if stat not in available_for:
                return current_app.response_class(
                    response=json.dumps({}),
                    status=status.HTTP_403_FORBIDDEN,
                    mimetype='application/json'
                )
            return func(*args, **kwargs)
        return decorated_function
    return actual_decorator
