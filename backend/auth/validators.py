from cerberus import Validator
from api_validation.validation_utils import add_http_method_validation
from api_validation.validation_callback import METHOD_FIELD


validator_main: Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_register: Validator = Validator({
    METHOD_FIELD: {
        'required': True,
        'type': 'string',
        'anyof': [
            {'allowed': ["GET"]},
            {'allowed': ["POST"], 'dependencies': ['email', 'psw']}
        ]
    },
    'email': {'required': False, 'type': 'string', 'regex': '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', 'maxlength': 128},
    'psw': {'required': False, 'type': 'string', 'minlength': 8}
})

validator_verify: Validator = Validator( add_http_method_validation( ["GET", "POST"], { } ) )

validator_confirm: Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_login: Validator = Validator({
    METHOD_FIELD: {
        'required': True,
        'type': 'string',
        'anyof': [
            {'allowed': ["GET"]},
            {'allowed': ["POST"], 'dependencies': ['email', 'psw']}
        ]
    },
    'email': {'required': False, 'type': 'string', 'regex': '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', 'maxlength': 128},
    'psw': {'required': False, 'type': 'string', 'minlength': 8},
    'forwarded_route': {'required': False, 'type': 'string'},
})

validator_check: Validator = Validator( add_http_method_validation( ["POST"], { } ) )

validator_logout: Validator = Validator( add_http_method_validation( ["POST"], { } ) )
