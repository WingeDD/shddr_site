from werkzeug.security import generate_password_hash, check_password_hash
# from werkzeug.datastructures import Headers
from itsdangerous.exc import BadSignature, SignatureExpired
from flask import request, json, current_app, url_for, render_template
from flask_login import login_user, logout_user, login_required, current_user
from flask_api import status
from db_handle.init import db
from db_handle.enums import UserField
from db_handle.db_tables import Users
from auth import auth, unauthorized
from auth.login_user import LoginUser
from sqlalchemy.exc import NoResultFound
from auth.token import VERIFICATION_TOKEN_EXPIRATION_TIME, generate_confirmation_token, confirm_token
from emailing.sending import send_email


@auth.route('', methods=['GET'])
@auth.route('/', methods=['GET'])
@auth.route("/main", methods=['GET'])
@auth.route("/index", methods=['GET'])
def main():
    return current_app.response_class(
        response=render_template('index.html'),
        status=status.HTTP_200_OK,
        mimetype='text/html'
    )


@auth.route('/register', methods=['GET', 'POST'])
def register():
    try:
        if request.method == 'GET':
            return current_app.response_class(
                response=render_template('index.html'),
                status=status.HTTP_200_OK,
                mimetype='text/html'
            )
        else:
            request_data: map = request.get_json() # email, psw
            request_data["email"] = request_data["email"].lower()
            exists: bool = db.session.execute( db.select(Users).where(getattr(Users, UserField.EMAIL) == request_data["email"]).exists().select() ).scalar_one()
            if exists:
                return current_app.response_class(
                    response=json.dumps({"email": request_data["email"]}),
                    status=status.HTTP_409_CONFLICT,
                    mimetype='application/json'
            )
            user: Users = Users(**{
                UserField.EMAIL: request_data["email"],
                UserField.PSW_HASH: generate_password_hash(request_data["psw"]),
                UserField.STATUS: current_app.config["DEFAULT_USER_STATUS"]
            })
            db.session.add(user)
            db.session.flush()
            db.session.refresh(user)
            login_user(LoginUser(user=user))
            db.session.commit()
            return current_app.response_class(
                response=json.dumps({"redirection_route": url_for('.verify')}),
                status=status.HTTP_302_FOUND,
                mimetype='application/json'
                # headers=Headers({"Location": url_for('.verify')})
            )
    except Exception as e:
        db.session.rollback()
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@auth.route('/verify', methods=['GET', 'POST'])
# @login_required
def verify():
    try:
        if request.method == 'GET':
            return current_app.response_class(
                response=render_template('index.html'),
                status=status.HTTP_200_OK,
                mimetype='text/html'
            )
        else:
            if current_user.is_authenticated:
                user: Users = current_user.get_user()
                email: str = getattr(user, UserField.EMAIL)
                token: str = generate_confirmation_token(email)
                confirm_url: str = url_for('.confirm', token=token, _external=True, _method="GET")
                html: str = render_template('verify_msg.html', confirm_url=confirm_url)
                subject: str = "Email confirmation"
                send_email(email, subject, html)
                return current_app.response_class(
                    response=json.dumps({"expiration_time": VERIFICATION_TOKEN_EXPIRATION_TIME}),
                    status=status.HTTP_200_OK,
                    mimetype='application/json'
                )
            else:
                return unauthorized()
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@auth.route('/confirm/<string:token>', methods=['GET']) # mailed link
# @login_required
def confirm(token: str):
    try:
        email: str = confirm_token(token)
        user: Users = db.session.execute(db.select(Users).where(getattr(Users, UserField.EMAIL) == email)).scalar_one()
        setattr(user, UserField.VERIFIED, True)
        db.session.commit()
        return current_app.response_class(
            response=render_template('index.html'),
            status=status.HTTP_200_OK,
            mimetype='text/html'
        )
    except SignatureExpired:
        return current_app.response_class(
            response=json.dumps({"token": token, "expiration_time": VERIFICATION_TOKEN_EXPIRATION_TIME}),
            status=status.HTTP_410_GONE,
            mimetype='application/json'
        )
    except BadSignature:
        return current_app.response_class(
            response=json.dumps({"token": token}),
            status=status.HTTP_404_NOT_FOUND,
            mimetype='application/json'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@auth.route('/login', methods=['GET', 'POST'])
def login():
    try:
        if request.method == 'GET':
            return current_app.response_class(
                response=render_template('index.html'),
                status=status.HTTP_200_OK,
                mimetype='text/html'
            )
        else:
            request_data: map = request.get_json() # email, psw, [forwarded_route]
            request_data["email"] = request_data["email"].lower()
            user: Users = db.session.execute(db.select(Users).where(getattr(Users, UserField.EMAIL) == request_data["email"])).scalar_one()
            matched: bool = check_password_hash(getattr(user, UserField.PSW_HASH), request_data["psw"])
            if not matched:
                return current_app.response_class(
                    response=json.dumps({"reason": "psw"}),
                    status=status.HTTP_401_UNAUTHORIZED,
                    mimetype='application/json'
                )
            login_user(LoginUser(user=user))
            if getattr(user, UserField.VERIFIED):
                if "forwarded_route" in request_data:
                    return current_app.response_class(
                        response=json.dumps({"redirection_route": request_data["forwarded_route"]}),
                        status=status.HTTP_302_FOUND,
                        mimetype='application/json',
                        # headers=Headers({"Location": request_data["forwarded_route"]})
                    )
                else:
                    return current_app.response_class(
                        response=json.dumps({"redirection_route": "/"}),
                        status=status.HTTP_302_FOUND,
                        mimetype='application/json',
                        # headers=Headers({"Location": "/"})
                    )
            else:
                return current_app.response_class(
                    response=json.dumps({"redirection_route": url_for('.verify')}),
                    status=status.HTTP_302_FOUND,
                    mimetype='application/json',
                    # headers=Headers({"Location": url_for('.verify')})
                )
    except NoResultFound:
        return current_app.response_class(
            response=json.dumps({"reason": "email"}),
            status=status.HTTP_401_UNAUTHORIZED,
            mimetype='application/json'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@auth.route('/check', methods=['POST'])
def check():
    try:
        if current_user.is_authenticated:
            user: Users = current_user.get_user()
            email: str = getattr(user, UserField.EMAIL)
            return current_app.response_class(
                response=json.dumps({"email": email}),
                status=status.HTTP_200_OK,
                mimetype='application/json'
            )
        else:
            return current_app.response_class(
                response=json.dumps({}),
                status=status.HTTP_401_UNAUTHORIZED,
                mimetype='application/json'
            )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@auth.route("/logout", methods=['POST'])
@login_required
def logout():
    try: 
        logout_user()
        return current_app.response_class(
            response=json.dumps({"redirection_route": url_for('.login')}),
            status=status.HTTP_302_FOUND,
            mimetype='application/json',
            # headers=Headers({"Location": url_for('.login')})
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )
