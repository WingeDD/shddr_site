from itsdangerous import URLSafeTimedSerializer
from flask import current_app


VERIFICATION_TOKEN_EXPIRATION_TIME: int = 3600


def generate_confirmation_token(email: str) -> str:
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=current_app.config['SECURITY_PASSWORD_SALT'])


def confirm_token(token: str, expiration: int = VERIFICATION_TOKEN_EXPIRATION_TIME) -> str:
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    return serializer.loads(
        token,
        salt=current_app.config['SECURITY_PASSWORD_SALT'],
        max_age=expiration
    )
