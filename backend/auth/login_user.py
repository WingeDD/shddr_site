from db_handle.init import db
from db_handle.enums import UserField, UserStatus
from db_handle.db_tables import Users


USER_DATA_FIELD: str = "__user"


class LoginUser:
    # __hash__ = object.__hash__

    def __init__(self, user_id: str = None, user: Users = None) -> None:
        if (hasattr(self, USER_DATA_FIELD)) and (getattr(self, USER_DATA_FIELD) is not None):
            pass
        elif user is not None:
            if (user_id is not None):
                if (user_id != getattr(user, UserField.ID)):
                    raise Exception("Both user_id and user args were given but user.id != user_id")
            setattr(self, USER_DATA_FIELD, user)
        elif user_id is not None:
            setattr(self, USER_DATA_FIELD, db.session.execute(db.select(Users).where(getattr(Users, UserField.ID) == int(user_id))).scalar_one())
        else:
            raise Exception("Can not construct LoginUser object without user_id or user args")

    @property
    def is_active(self) -> bool:
        return True

    @property
    def is_authenticated(self) -> bool:
        return self.is_active

    @property
    def is_anonymous(self) -> bool:
        return False

    def get_id(self) -> str:
        return str(getattr(getattr(self, USER_DATA_FIELD), UserField.ID))

    def get_email(self) -> str:
        return getattr(getattr(self, USER_DATA_FIELD), UserField.EMAIL)

    def get_psw_hash(self) -> str:
        return getattr(getattr(self, USER_DATA_FIELD), UserField.PSW_HASH)

    def get_status(self) -> UserStatus:
        return UserStatus( getattr(getattr(self, USER_DATA_FIELD), UserField.STATUS) )
        
    def get_verified(self) -> bool:
        return getattr(getattr(self, USER_DATA_FIELD), UserField.VERIFIED)

    @property
    def is_verified(self) -> bool:
        return self.get_verified()

    def get_user(self) -> Users:
        return getattr(self, USER_DATA_FIELD)
