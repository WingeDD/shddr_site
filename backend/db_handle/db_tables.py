from datetime import datetime
from db_handle.init import db
from db_handle.enums import UserStatus, UserField, ResultField, TaskStatus, ResultStatus


class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    verified = db.Column(db.Boolean, default=False)
    psw_hash = db.Column(db.String(256), nullable=False)
    status = db.Column(db.Enum(str(UserStatus.CANDIDATE), str(UserStatus.USER), str(UserStatus.ADMIN), name=UserStatus.__name__), nullable=False, unique=False, default=str(UserStatus.CANDIDATE))
    def __repr__(self) -> str:
        return f"<{Users.__name__} {self.id}>"

class Tasks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner = db.Column(db.Integer, db.ForeignKey(f'{Users.__name__.lower()}.{UserField.ID.value}'), nullable=False, unique=False) # required
    name = db.Column(db.String(128), unique=False, nullable=True, default=None)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow) # date of task creation
    param_iper = db.Column(db.Integer, nullable=False, unique=False) # required
    param_gap_cost = db.Column(db.Integer, unique=False, nullable=False) # required
    data = db.Column(db.Text, nullable=False) # required
    status = db.Column(db.Enum(str(TaskStatus.QUEUED), str(TaskStatus.EXECUTING), str(TaskStatus.DONE), name=UserStatus.__name__), nullable=False, unique=False, default=str(TaskStatus.QUEUED)) 
    def __repr__(self) -> str:
        return f"<{Tasks.__name__} {self.id}>"

class Results(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner = db.Column(db.Integer, db.ForeignKey(f'{Users.__name__.lower()}.{UserField.ID.value}'), nullable=False, unique=False) # required
    name = db.Column(db.String(128), unique=False, nullable=True, default=None)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow) # date of last status change (anyway it is date of creation)
    status = db.Column(db.Enum(str(ResultStatus.ERROR), str(ResultStatus.OK), name=UserStatus.__name__), nullable=False, unique=False)
    def __repr__(self) -> str:
        return f"<{Results.__name__} {self.id}>"

class ResultsData(db.Model):
    result_id = db.Column(db.Integer, db.ForeignKey(f'{Results.__name__.lower()}.{ResultField.ID.value}'), nullable=False, unique=False, primary_key=True) # required
    name = db.Column(db.String(32), unique=False, nullable=False, primary_key=True)
    data = db.Column(db.Text, nullable=False)
    def __repr__(self) -> str:
        return f"<{ResultsData.__name__} {self.result_id} {self.name}>"
