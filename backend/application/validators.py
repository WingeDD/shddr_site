from cerberus import Validator
from api_validation.validation_utils import add_http_method_validation


validator_main: Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_about: Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_result_page: Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_result_list: Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_result_fetch : Validator = Validator( add_http_method_validation( ["POST"], {
    'id': {'required': True, 'type': 'integer'}
} ) )

validator_task_list : Validator = Validator( add_http_method_validation( ["GET"], { } ) )

validator_task_fetch : Validator = Validator( add_http_method_validation( ["POST"], {
    'id': {'required': True, 'type': 'integer'}
} ) )

validator_task_put : Validator = Validator( add_http_method_validation( ["PUT"], {
    'name': {'required': False, 'type': 'string', 'maxlength': 128},
    'param_iper': {'required': True, 'type': 'integer', 'min': 100, 'max': 800},
    'param_gap_cost': {'required': False, 'type': 'integer', 'min': 0, 'max': 10000},
    'data': {'required': True}
} ) )
