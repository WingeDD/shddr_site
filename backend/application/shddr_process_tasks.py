#!/bin/python3


from typing import Optional
import os
import sys
import time
import subprocess
import datetime
import re
from sqlalchemy import create_engine, select, delete, update, func
from sqlalchemy.orm import sessionmaker

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from db_handle.db_tables import Tasks, Results, ResultsData
from db_handle.enums import TaskField, ResultField, ResultsDataField, TaskStatus, ResultStatus
from db_handle.utils import row2dict
import application.application_config as cfg
from config import SQLALCHEMY_DATABASE_URI


LAUNCHER_PATH: str = os.path.join(cfg.PROCESSING_PROXY_DIR, cfg.PROCESSING_LAUNCHER)  
LOG_PATH: str = os.path.join(SCRIPT_DIR, cfg.PROCESSING_LOOP_SCRIPT_LOG)
INPUT_PROXY_PATH: str = os.path.join(cfg.PROCESSING_PROXY_DIR, cfg.PROCESSING_INPUT_FILE)
PARAM_IPER_PROXY_PATH: str = os.path.join(cfg.PROCESSING_PROXY_DIR, cfg.PROCESSING_PARAM_IPER_FILE)
PARAM_GAP_COST_PROXY_PATH: str = os.path.join(cfg.PROCESSING_PROXY_DIR, cfg.PROCESSING_PARAM_GAP_COST_FILE)
RES_PROXY_DIR: str = os.path.join(cfg.PROCESSING_PROXY_DIR, cfg.PROCESSING_PROXY_RES_SUBDIR)
INPUT_PATH: str = os.path.join(cfg.PROCESSING_DIR, cfg.PROCESSING_INPUT_FILE)
PARAM_IPER_PATH: str = os.path.join(cfg.PROCESSING_DIR, cfg.PROCESSING_PARAM_IPER_FILE)
PARAM_GAP_COST_PATH: str = os.path.join(cfg.PROCESSING_DIR, cfg.PROCESSING_PARAM_GAP_COST_FILE)


re_sbatch_access_slurm_resp = re.compile(r'^\s*Submitted\s*batch\s*job\s*(\d+)\s*$', flags=re.RegexFlag.IGNORECASE)
re_squeue_failure_slurm_resp = re.compile(r'.*invalid\s*job\s*id.*', flags=re.RegexFlag.IGNORECASE)
re_bad_slurm_out = re.compile(r'(?:terminated|error|fail)', flags=re.RegexFlag.IGNORECASE)


def get_and_set_task_executing(session, logfile) -> Optional[dict]:
    min_date: Optional[int] = session.execute( select(func.min(Tasks.date)).where(getattr(Tasks, TaskField.STATUS) == TaskStatus.QUEUED.value) ).scalar_one() # date or None
    if min_date is None:
        return None
    row = session.execute(select(Tasks).where(getattr(Tasks, TaskField.DATE) == min_date)).one()
    res = {
        TaskField.ID: getattr(row.Tasks, TaskField.ID),
        TaskField.DATE: getattr(row.Tasks, TaskField.DATE),
        TaskField.OWNER: getattr(row.Tasks, TaskField.OWNER),
        TaskField.NAME: getattr(row.Tasks, TaskField.NAME)
    }
    try:
        session.execute( update(Tasks).where(getattr(Tasks, TaskField.ID) == res[TaskField.ID]).values( **{TaskField.STATUS.value: TaskStatus.EXECUTING.value} ) )
        with open(INPUT_PROXY_PATH, 'w') as file:
            file.write(getattr(row.Tasks, TaskField.DATA))
        with open(PARAM_IPER_PROXY_PATH, 'w') as file:
            params: str = "NOM=    21  IPER=%6d\n" % getattr(row.Tasks, TaskField.PARAM_IPER)
            file.write(params)
        with open(PARAM_GAP_COST_PROXY_PATH, 'w') as file:
            params: str = f"{getattr(row.Tasks, TaskField.PARAM_GAP_COST)}"
            file.write(params)
    except Exception as e:
        logfile.write(f"In get_and_set_task_executing: {repr(e)}\n")
        create_result_err_set_task_done(session, res)
        raise e
    return res


def create_result(session, task_properties: set, status: ResultStatus) -> None:
    res: Results = Results()
    setattr(res, ResultField.ID, task_properties[TaskField.ID])
    setattr(res, ResultField.OWNER, task_properties[TaskField.OWNER])
    setattr(res, ResultField.NAME, task_properties[TaskField.NAME])
    setattr(res, ResultField.STATUS, status.value)
    session.add(res)


def create_result_ok_set_task_done(session, task_properties: set) -> None:
    id: int = task_properties[TaskField.ID]
    session.execute( update(Tasks).where(getattr(Tasks, TaskField.ID) == id).values( **{TaskField.STATUS.value: TaskStatus.DONE.value} ) )
    create_result(session, task_properties, ResultStatus.OK)
    with open(os.path.join(RES_PROXY_DIR, cfg.PROCESSING_RESULT1), 'r') as file:
        res_data = ResultsData()
        setattr(res_data, ResultsDataField.RESULT_ID, id)
        setattr(res_data, ResultsDataField.NAME, cfg.PROCESSING_RESULT1)
        setattr(res_data, ResultsDataField.DATA, file.read())
        session.add(res_data)
    for idx in range(cfg.PROCESSING_RESULT2_FROM, cfg.PROCESSING_RESULT2_TO+1):
        fname: str = f"{cfg.PROCESSING_RESULT2_BASE}{idx}{cfg.PROCESSING_RESULT2_EXT}"
        with open(os.path.join(RES_PROXY_DIR, fname), 'r') as file:
            res_data = ResultsData()
            setattr(res_data, ResultsDataField.RESULT_ID, id)
            setattr(res_data, ResultsDataField.NAME, fname)
            setattr(res_data, ResultsDataField.DATA, file.read())
            session.add(res_data)


def create_result_err_set_task_done(session, task_properties: set) -> None:
    id: int = task_properties[TaskField.ID]
    create_result(session, task_properties, ResultStatus.ERROR)
    session.execute( update(Tasks).where(getattr(Tasks, TaskField.ID) == id).values( **{TaskField.STATUS.value: TaskStatus.DONE.value} ) )


def set_result_err(session, id: int):
    session.execute( update(Results).where(getattr(Results, ResultField.ID) == id).values( **{ResultField.STATUS.value: ResultStatus.ERROR.value} ) )

"slurm-%j.out"
def make_launcher_slurm() -> None:
    with open(LAUNCHER_PATH, 'w') as file:
        file.write(
f"""#!/bin/bash
#
#SBATCH --partition={cfg.SLURM_PARTITION}
#SBATCH --chdir={cfg.PROCESSING_DIR}
#SBATCH --output={os.path.join(cfg.PROCESSING_PROXY_DIR, 'slurm-%j.out')}
#SBATCH --error={os.path.join(cfg.PROCESSING_PROXY_DIR, 'slurm-%j.out')}
#SBATCH --nodes={cfg.SLURM_NODES}
#SBATCH --ntasks-per-node={cfg.SLURM_NTASKS_PER_NODE}
#SBATCH --ntasks-per-socket={cfg.SLURM_NTASKS_PER_SOCKET}
#SBATCH --cpus-per-task={cfg.SLURM_CPUS_PER_TASK}
#SBATCH --time={cfg.SLURM_TIME}\n\n
rm -rf {cfg.PROCESSING_DIR}/group*
rm -rf {cfg.PROCESSING_DIR}/start*
rm -rf {INPUT_PATH}
rm -rf {PARAM_IPER_PATH}
rm -rf {PARAM_GAP_COST_PATH}
rm -rf {cfg.PROCESSING_SUBDIR}/{cfg.PROCESSING_RESULT1}
rm -rf {cfg.PROCESSING_SUBDIR}/{cfg.PROCESSING_RESULT2_BASE}*{cfg.PROCESSING_RESULT2_EXT}\n
cp -f {INPUT_PROXY_PATH} {INPUT_PATH}
cp -f {PARAM_IPER_PROXY_PATH} {PARAM_IPER_PATH}
cp -f {PARAM_GAP_COST_PROXY_PATH} {PARAM_GAP_COST_PATH}\n
cd {cfg.PROCESSING_DIR}
./{cfg.PROCESSING_SCRIPT_1} &> /dev/null
cd {cfg.PROCESSING_SUBDIR}
./{cfg.PROCESSING_SCRIPT_2} &> /dev/null\n
cp -f {cfg.PROCESSING_SUBDIR}/{cfg.PROCESSING_RESULT1} {RES_PROXY_DIR}
cp -f {cfg.PROCESSING_SUBDIR}/{cfg.PROCESSING_RESULT2_BASE}*{cfg.PROCESSING_RESULT2_EXT} {RES_PROXY_DIR}
"""
        )


def launch_slurm() -> int:
    proc = subprocess.Popen(['ssh', cfg.CLUSTER_MANAGER_NODE, "sbatch", LAUNCHER_PATH], cwd=cfg.PROCESSING_PROXY_DIR, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    rcode: int = proc.wait()
    if rcode != 0:
        raise Exception(f"sbatch return code: {rcode}, stdout: {proc.stdout.read().decode('utf-8')}, stderr: {proc.stderr.read().decode('utf-8')}")
    out: str = (proc.stdout.read()).decode("utf-8")
    parsed = re_sbatch_access_slurm_resp.search(out)
    if parsed is None:
        raise Exception(f"sbatch stdout: {out}, stderr: {(proc.stderr.read()).decode('utf-8')}")
    return int(parsed[1])


def wait_slurm(task_id: int) -> None:
    while True:
        time.sleep(cfg.SLURM_WAIT_SLEEP_TIME)
        proc = subprocess.Popen(['ssh', cfg.CLUSTER_MANAGER_NODE, "squeue", "-j", str(task_id)], cwd=cfg.PROCESSING_PROXY_DIR, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        rcode: int = proc.wait()
        out: str = (proc.stdout.read()).decode("utf-8") 
        err: str = (proc.stderr.read()).decode("utf-8") 
        if rcode != 0 and re_squeue_failure_slurm_resp.match(err):
            break
        elif rcode != 0:
            raise Exception(f"squeue return code: {rcode}, stdout: {out}, stderr: {err}")
        # return code == 0
        lines: int = 0
        for sym in out:
            if sym == '\n':
                lines += 1
        # header + [job record]
        if lines < 2:
            break


def check_and_clear(task_id: int) -> None:
    slurm_out_file_path: str = os.path.join(cfg.PROCESSING_PROXY_DIR, f"slurm-{task_id}.out")
    if os.path.exists(slurm_out_file_path):
        with open(slurm_out_file_path, 'r') as file:
            if (re_bad_slurm_out.search(file.read())):
                raise Exception(f"Error: found error in {slurm_out_file_path}; task_id: {task_id}")
        os.remove(slurm_out_file_path)
    if os.path.exists(INPUT_PROXY_PATH):
        os.remove(INPUT_PROXY_PATH)
    if os.path.exists(PARAM_IPER_PROXY_PATH):
        os.remove(PARAM_IPER_PROXY_PATH)
    if os.path.exists(PARAM_GAP_COST_PROXY_PATH):
        os.remove(PARAM_GAP_COST_PROXY_PATH)
    pr1: str = os.path.join(RES_PROXY_DIR, cfg.PROCESSING_RESULT1)
    if os.path.exists(pr1):
        os.remove(pr1)
    for idx in range(cfg.PROCESSING_RESULT2_FROM, cfg.PROCESSING_RESULT2_TO+1):
        fname: str = f"{cfg.PROCESSING_RESULT2_BASE}{idx}{cfg.PROCESSING_RESULT2_EXT}"
        fpath: str = os.path.join(RES_PROXY_DIR, fname)
        if os.path.exists(fpath):
            os.remove(fpath)


def delete_old(session, logfile) -> None:
    threshold_date = datetime.datetime.utcnow() - datetime.timedelta(days=cfg.PROCESSING_RESULT_STORE_TIME)
    old_results = session.execute(select(Results).where(getattr(Results, ResultField.DATE) < threshold_date)).all()
    for old_result in old_results:
        id = getattr(old_result.Results, ResultField.ID)
        session.execute( delete(ResultsData).where(getattr(ResultsData, ResultsDataField.RESULT_ID) == id) )
        session.execute( delete(Results).where(getattr(Results, ResultField.ID) == id) )
        session.execute( delete(Tasks).where(getattr(Tasks, TaskField.ID) == id) )
        logfile.write(f"Result deleted: {row2dict(old_result.Results)}\n")
        session.commit()


def launch() -> None:
    while True:
        time.sleep(cfg.PROCESSING_SLEEP_TIME)

        # set logging
        try:
            log = open(LOG_PATH, "a")
        except Exception as e:
            print(repr(e))
            continue
        
        # set db
        try:
            engine = create_engine(SQLALCHEMY_DATABASE_URI)
            Sessionmaker = sessionmaker(engine)
        except Exception as e:
            log.write(f"{repr(e)}\n")
            log.close()
            continue

        # delete old
        try:
            session = Sessionmaker()
        except Exception as e:
            log.write(f"{repr(e)}\n")
            log.close()
            continue

        try:
            delete_old(session, log) # commits itself
        except Exception as e:
            log.write(f"{repr(e)}\n")
        finally:
            session.close()


        # getting task info
        try:
            session = Sessionmaker()
        except Exception as e:
            log.write(f"{repr(e)}\n")
            log.close()
            continue

        try:
            task_properties: Optional[dict] = get_and_set_task_executing(session, log)
            session.commit()
            session.close()
        except Exception as e:
            # in case of error db state handled inside the func
            session.commit()
            session.close()
            log.write(f"{repr(e)}\n")
            log.close()
            continue


        # if no task (no way to handle exceptions here)
        if task_properties is None:
            log.close()
            continue


        # cluster computing
        try:
            make_launcher_slurm()
            slurm_task_id: int = launch_slurm()
            wait_slurm(slurm_task_id)
        except Exception as e:
            try:
                session = Sessionmaker()
            except Exception as e2:
                log.write(f"{repr(e)}\n")
                log.write(f"{repr(e2)}\n")
                log.close()
                continue
            create_result_err_set_task_done(session, task_properties)
            session.commit()
            session.close()
            log.write(f"{repr(e)}\n")
            log.close()
            continue


        # results setting and checking
        try:
            session = Sessionmaker()
        except Exception as e:
            log.write(f"{repr(e)}\n")
            log.close()
            continue

        try:
            create_result_ok_set_task_done(session, task_properties)
            check_and_clear(slurm_task_id)
            session.commit()
            log.write(f"Task completed: {task_properties}\n")
        except Exception as e:
            set_result_err(session, task_properties[TaskField.ID])
            session.commit()
            log.write(f"{repr(e)}\n")
        finally:
            session.commit()
            session.close()
            log.close()



if __name__ == "__main__":
    while True:
        try:
            launch()
        except:
            pass
