from flask import current_app, render_template, request, json
from flask_login import login_required, current_user
from flask_api import status
from sqlalchemy.exc import NoResultFound
from auth.decorators import is_verified, status_required
from application import application
from db_handle.enums import UserStatus, TaskField, ResultField, ResultsDataField
from db_handle.db_tables import Tasks, Results, ResultsData
from db_handle.init import db
from db_handle.utils import row2dict
from application.tasks_processing import check_tasks_processor, validate_and_normalize_input
import application.application_config as cfg


@application.route("", methods=['GET'])
@application.route("/", methods=['GET'])
@application.route("/main", methods=['GET'])
@application.route("/index", methods=['GET'])
def main():
    try:
        return current_app.response_class(
            response=render_template('index.html'),
            status=status.HTTP_200_OK,
            mimetype='text/html'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/about", methods=['GET'])
def about():
    try:
        return current_app.response_class(
            response=render_template('index.html'),
            status=status.HTTP_200_OK,
            mimetype='text/html'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/result/page/<int:id>", methods=['GET'])
def result_page(id):
    try:
        return current_app.response_class(
            response=render_template('index.html'),
            status=status.HTTP_200_OK,
            mimetype='text/html'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/result/list", methods=['GET'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def result_list():
    try:
        user_id: int = int(current_user.get_id())
        rows = db.session.execute(db.select(Results.id, Results.name, Results.date, Results.status).where(getattr(Results, ResultField.OWNER) == user_id)).all()
        results: list[dict] = list(map(lambda row: row._asdict(), rows))
        return current_app.response_class(
            response=json.dumps(results),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/result/fetch", methods=['POST'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def result_fetch():
    try:
        request_data: map = request.get_json(force=True) # id
        result_id: int = request_data['id']
        try:
            result = db.session.execute(db.select(Results).where(getattr(Results, ResultField.ID) == result_id)).one()
        except NoResultFound:
            return current_app.response_class(
                response=json.dumps({}),
                status=status.HTTP_410_GONE,
                mimetype='application/json'
            )
        user_id: int = int(current_user.get_id())
        user_status: UserStatus = current_user.get_status()
        if (getattr(result.Results, ResultField.OWNER) != user_id) and (user_status != UserStatus.ADMIN):
            return current_app.response_class(
                response=json.dumps({"reason": "ownership"}),
                status=status.HTTP_403_FORBIDDEN,
                mimetype='application/json'
            )
        result_data: ResultsData = db.session.execute(db.select(ResultsData.name, ResultsData.data).where(getattr(ResultsData, ResultsDataField.RESULT_ID) == result_id)).all()
        res = row2dict(result.Results)
        del res[ResultField.OWNER]
        return current_app.response_class(
            response=json.dumps({"result": res, "result_data": list(map(lambda row: row._asdict(), result_data))}),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/task/list", methods=['GET'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def task_list():
    try:
        user_id: int = int(current_user.get_id())
        rows = db.session.execute(db.select(Tasks.id, Tasks.name, Tasks.date, Tasks.param_iper, Tasks.param_gap_cost, Tasks.status).where(getattr(Tasks, TaskField.OWNER) == user_id)).all()
        results: list[dict] = list(map(lambda row: row._asdict(), rows))
        return current_app.response_class(
            response=json.dumps(results),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )
    

@application.route("/task/fetch", methods=['POST'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def task_fetch():
    try:
        request_data: map = request.get_json(force=True) # id
        task_id: int = request_data['id']
        try:
            task = db.session.execute(db.select(Tasks.id, Tasks.owner, Tasks.name, Tasks.date, Tasks.param_iper, Tasks.param_gap_cost, Tasks.status).where(getattr(Tasks, TaskField.ID) == task_id)).one()
        except NoResultFound:
            return current_app.response_class(
                response=json.dumps({}),
                status=status.HTTP_410_GONE,
                mimetype='application/json'
            )
        user_id: int = int(current_user.get_id())
        user_status: UserStatus = current_user.get_status()
        if (getattr(task, TaskField.OWNER) != user_id) and (user_status != UserStatus.ADMIN):
            return current_app.response_class(
                response=json.dumps({"reason": "ownership"}),
                status=status.HTTP_403_FORBIDDEN,
                mimetype='application/json'
            )
        res = task._asdict()
        del res[TaskField.OWNER]
        return current_app.response_class(
            response=json.dumps(res),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )


@application.route("/task", methods=['PUT'])
@login_required
@is_verified
@status_required([UserStatus.USER, UserStatus.ADMIN])
def task_put():
    try:
        request_data: map = request.get_json(force=True) # [name], data, param_iper, [param_gap_cost]
        user_id: int = int(current_user.get_id())
        user_status: UserStatus = current_user.get_status()
        tasks_count: int = db.session.execute( db.select(db.func.count()).select_from(Tasks).where(getattr(Tasks, TaskField.OWNER) == user_id) ).scalar_one()
        results_count: int = db.session.execute( db.select(db.func.count()).select_from(Results).where(getattr(Results, ResultField.OWNER) == user_id) ).scalar_one()
        task_limit: int = cfg.MAX_ADMIN_TASKS if user_status == UserStatus.ADMIN else cfg.MAX_USER_TASKS
        if (tasks_count-results_count) >= task_limit:
            return current_app.response_class(
                response=json.dumps({"reason": "count", "count": tasks_count, "limit": task_limit}),
                status=status.HTTP_403_FORBIDDEN,
                mimetype='application/json'
            )
        
        data, seq_name, issue = validate_and_normalize_input(request_data['data'])
        if issue is not None:
            return current_app.response_class(
                response=json.dumps({"reason": "sequence format", "issue": issue}),
                status=status.HTTP_400_BAD_REQUEST,
                mimetype='application/json'
            )
        ok = check_tasks_processor()
        if not ok:
            return current_app.response_class(
                response=json.dumps({"reason": "tasks processor is turned off (please contact us to fix this)"}),
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                mimetype='application/json'
            )
        
        task: Tasks = Tasks()
        setattr(task, TaskField.OWNER, user_id)
        setattr(task, TaskField.NAME, request_data['name'] if 'name' in request_data else (seq_name if seq_name else None))
        setattr(task, TaskField.PARAM_IPER, request_data['param_iper'])
        setattr(
            task,
            TaskField.PARAM_GAP_COST,
            request_data['param_gap_cost'] if (('param_gap_cost' in request_data) and (type(request_data['param_gap_cost']) == int)) else cfg.PROCESSING_DEFAULT_GAP_COST
        )
        setattr(task, TaskField.DATA, data)
        db.session.add(task)
        db.session.flush()
        task_id: int = getattr(task, TaskField.ID)
        db.session.commit()
        return current_app.response_class(
            response=json.dumps({"id": task_id, "store_time": cfg.PROCESSING_RESULT_STORE_TIME}),
            status=status.HTTP_200_OK,
            mimetype='application/json'
        )
    except Exception as e:
        db.session.rollback()
        return current_app.response_class(
            response=json.dumps(repr(e)),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            mimetype='application/json'
        )
