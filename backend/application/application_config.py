DAEMON_NAME: str = "shddr.service"

MAX_USER_TASKS: int = 3
MAX_ADMIN_TASKS: int = 100
CLUSTER_MANAGER_NODE: str = "masternode"


MAX_ROW_LENGTH: int = 60
MAX_NAME_LENGTH: int = 128
MIN_SEQ_LENGTH: int = 500000
MAX_SEQ_LENGTH: int = 20000000


PROCESSING_SLEEP_TIME: int = 5
PROCESSING_LOOP_SCRIPT: str = "shddr_process_tasks.py"
PROCESSING_LOOP_SCRIPT_LOG: str = "shddr_process_tasks.log"
PROCESSING_PROXY_DIR: str = "/pool_hot/shddr_site/Rice/dispersed/scan_avto/ecoli_n11/proxy"
PROCESSING_PROXY_RES_SUBDIR: str = "result"
PROCESSING_DIR: str = "/home/eugene/Документы/Rice/dispersed/scan_avto/ecoli_m11"
PROCESSING_LAUNCHER: str = "shddr_task_processing.sh"
PROCESSING_SCRIPT_1: str = "run_n1"
PROCESSING_SUBDIR: str = "/home/eugene/Документы/Rice/dispersed/scan_avto/ecoli_m11/genome_un"
PROCESSING_SCRIPT_2: str = "run_select"
PROCESSING_INPUT_FILE: str = "ecoli_k12.fasta"
PROCESSING_PARAM_IPER_FILE: str = "nom.txt"
PROCESSING_PARAM_GAP_COST_FILE: str = "vdel.txt"
PROCESSING_RESULT1: str = "uni_klass.txt"
PROCESSING_RESULT2_BASE: str = "uni"
PROCESSING_RESULT2_EXT: str = ".txt"
PROCESSING_RESULT2_FROM: int = 11
PROCESSING_RESULT2_TO: int = 30
PROCESSING_RESULT_STORE_TIME: int = 7 # days
PROCESSING_DEFAULT_GAP_COST: int = 35


SLURM_WAIT_SLEEP_TIME: int = 5
SLURM_PARTITION: str = "singler"
SLURM_NODES: int = 1
SLURM_NTASKS_PER_NODE: int = 1
SLURM_NTASKS_PER_SOCKET: int = 1
SLURM_CPUS_PER_TASK: int = 32
SLURM_TIME: str = "240:00:00"
