from typing import Optional, Tuple, Any
import os
import gc
import psutil
import subprocess
import re
from enum import Enum
import application.application_config as cfg


class ParsingState(Enum):
    SEQ_NAME_OR_HEADER1 = 0
    HEADER1 = 1
    SEQUENCES_OR_HEADER2 = 2
    FINISHED = 3
ignore_symbols: set = set((" ", "\t", "\n", "\r", "\v", "\f", "*"))
re_any_space = re.compile(r'^\s*$')
re_fasta_header = re.compile(r'^\s*>\s*(\S*).*$')
NEW_LINE_CODE: int = ord("\n")
symbols_normalization: dict[str, int] = {
    "n": ord("N"),
    "a": ord("A"),
    "t": ord("T"),
    "c": ord("C"),
    "g": ord("G"),
    "N": ord("N"),
    "A": ord("A"),
    "T": ord("T"),
    "C": ord("C"),
    "G": ord("G"),
}


def validate_and_normalize_input(input: str, encoding: str = 'ascii') -> Tuple[Optional[str], Optional[str], Optional[Any]]:
    """
    WARNING: the function deallocates input object!
    gets input splitted by '\\n'\n
    returns [normalized input], [sequence name], [error data convertible to json]
    """ 

    input_lines: list[str] = input.split("\n")
    del input
    gc.collect()

    res = bytearray()
    seq_name: Optional[str] = None
    state: ParsingState = ParsingState.SEQ_NAME_OR_HEADER1

    sym_counter: int = 0
    got_header2: bool = False

    try:
        for line in input_lines:
            if re_any_space.match(line):
                continue

            if state == ParsingState.SEQUENCES_OR_HEADER2:
                for sym in line:
                    if sym in ignore_symbols:
                        continue
                    if sym in symbols_normalization:
                        res.append(symbols_normalization[sym])
                        sym_counter += 1
                        if sym_counter % cfg.MAX_ROW_LENGTH == 0:
                            res.append(NEW_LINE_CODE)
                    else:
                        parsed = re_fasta_header.search(line) 
                        if parsed:
                            if res[len(res)-1] != NEW_LINE_CODE:
                                res.append(NEW_LINE_CODE)
                            for sym in line:
                                res.append(ord(sym))
                            res.append(NEW_LINE_CODE)
                            got_header2 = True
                            state = ParsingState.FINISHED
                            break
                        else:
                            return None, None, {"expected": "sequence OR fasta header (>...)", "got": line}
            elif state == ParsingState.SEQ_NAME_OR_HEADER1:
                parsed = re_fasta_header.search(line)
                if parsed:
                    seq_name = parsed[1] if len(parsed[1]) <= cfg.MAX_NAME_LENGTH else parsed[1][:cfg.MAX_NAME_LENGTH]
                    for sym in line:
                        res.append(ord(sym))
                    res.append(NEW_LINE_CODE)
                    state = ParsingState.SEQUENCES_OR_HEADER2
                else:
                    seq_name = line if len(line) <= cfg.MAX_NAME_LENGTH else line[:cfg.MAX_NAME_LENGTH]
                    for idx in range(min(len(line), cfg.MAX_ROW_LENGTH)):
                        res.append(ord(line[idx]))
                    res.append(NEW_LINE_CODE)
                    state = ParsingState.HEADER1
            elif state == ParsingState.HEADER1:
                parsed = re_fasta_header.search(line)
                if parsed:
                    if seq_name is None:
                        seq_name = parsed[1] if len(parsed[1]) <= cfg.MAX_NAME_LENGTH else parsed[1][:cfg.MAX_NAME_LENGTH]
                    for sym in line:
                        res.append(ord(sym))
                    res.append(NEW_LINE_CODE)
                    state = ParsingState.SEQUENCES_OR_HEADER2
                else:
                    return None, None, {"expected": "fasta header (>...)", "got": line}
            elif state == ParsingState.FINISHED:
                for sym in line:
                    if sym not in ignore_symbols:
                        return None, None, {"expected": "whitespace OR end of file", "got": line}
            else:
                return None, None, "Unexpected parsing state (possible bug)"

    except ValueError as e:
        if str(e).find("byte") != -1:
            return None, None, "File contains unsupported symbol (only symbols in Unicode range(0, 256) are supported)"
        else:
            raise e
        
    if sym_counter < cfg.MIN_SEQ_LENGTH or sym_counter > cfg.MAX_SEQ_LENGTH:
        return None, None, {"min sequence length": cfg.MIN_SEQ_LENGTH, "max sequence length": cfg.MAX_SEQ_LENGTH, "sequence length got": sym_counter}

    if not got_header2:
        if res[len(res)-1] != NEW_LINE_CODE:
            res.append(NEW_LINE_CODE)
        res.extend(bytes(f">{seq_name}\n", encoding))
    
    del input_lines
    gc.collect()
    return res.decode(encoding), seq_name, None


def task_processor_start() -> bool:
    path: str = os.path.realpath(os.path.dirname(__file__))
    proc = subprocess.Popen(['sudo', 'systemctl', 'restart', cfg.DAEMON_NAME], cwd=path)
    rcode: int = proc.wait()
    if rcode == 0:
        return True
    else:
        return False


def task_processor_status() -> bool:
    for p in psutil.process_iter():
        cmdline = p.cmdline()
        if len(cmdline) >= 2 and (cmdline[0].find("python") != -1) and (cmdline[1].find(cfg.PROCESSING_LOOP_SCRIPT) != -1):
            return True
    return False


def check_tasks_processor() -> bool:
    try:
        if not task_processor_status():
            return task_processor_start()
        else:
            return True
    except Exception as e:
        print(repr(e))
        return False
