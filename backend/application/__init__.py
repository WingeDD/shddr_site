from flask import Blueprint
from api_validation.validation_callback import validation_callback


application: Blueprint = Blueprint('application', __name__, static_url_path='', static_folder='static', template_folder='templates')
@application.before_request
def wrapper():
    return validation_callback()


from application import routes
