from flask import current_app
from flask_mail import Message
from emailing.init import mail


def send_email(to: str, subject: str, template: str):
    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=current_app.config['MAIL_DEFAULT_SENDER']
    )
    mail.send(msg)
