from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_login import login_required
from db_handle.init import db
from db_handle.db_tables import Users, Tasks, Results, ResultsData
from db_handle.enums import UserStatus
from auth.decorators import is_verified, status_required


class SecureModelView(ModelView):
    __none_func = lambda x: None

    def is_accessible(self):
        err_resp = login_required(self.__none_func)()
        if err_resp is not None:
            self.__err_resp = err_resp
            return False
        err_resp = is_verified(self.__none_func)()
        if err_resp is not None:
            self.__err_resp = err_resp
            return False
        err_resp = status_required([UserStatus.ADMIN])(self.__none_func)()
        if err_resp is not None:
            self.__err_resp = err_resp
            return False
        return True

    def inaccessible_callback(self, name, **kwargs):
        return self.__err_resp


admin: Admin = Admin(name = 'admin', template_mode='bootstrap4')
admin.add_view(SecureModelView(Users, db.session))
admin.add_view(SecureModelView(Tasks, db.session))
admin.add_view(SecureModelView(Results, db.session))
admin.add_view(SecureModelView(ResultsData, db.session))
