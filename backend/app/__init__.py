import os
from flask import Flask, send_from_directory
from db_handle.init import db, migrate
from emailing.init import mail
from auth import auth, login_manager
from application import application
from app.admin import admin


CONFIG_MODULE: str = "config"


app: Flask = Flask(__name__)
app.config.from_object(CONFIG_MODULE)
if not app.config['USING_EXTERNAL_WEBSERVER']:
    from app.prefix_middleware import PrefixMiddleware
    app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix=app.config['APPLICATION_ROOT'])
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(application.root_path, application.template_folder), 'favicon.ico')

db.init_app(app)
migrate.init_app(app, db)

mail.init_app(app)

app.register_blueprint(auth, url_prefix='/auth/')
login_manager.init_app(app)

admin.init_app(app)

app.register_blueprint(application, url_prefix='/')
