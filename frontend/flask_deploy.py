#!/usr/bin/python3
from typing import Optional
import os
import sys
import json
import shutil
import subprocess


__script_dir: str = os.path.dirname(os.path.abspath(__file__))
__default_config_file: str = os.path.join(__script_dir, r"flask_deploy.config.json")

def get_config_file(default: str=None) -> str:
    if len(sys.argv) > 2:
        raise Exception(f'expected 0 or 1 command line argument, got: {len(sys.argv)-1}.')
    elif len(sys.argv) == 2:
        return sys.argv[1]
    else:
        return default

def get_json_data(fname: str) -> dict:
    with open(fname, "r") as file:
        return json.load(file)


def check_files(*fpaths: str) -> Optional[list[str]]:
    bad_paths: list[str] = []
    for fpath in fpaths:
        if (not os.path.exists(fpath)) or (not os.path.isfile(fpath)):
            bad_paths.append(fpath)
    return bad_paths if len(bad_paths) else None

def check_directories(*dirs: str, create: bool = False) -> Optional[list[str]]:
    bad_paths: list[str] = []
    if create:
        for dir in dirs:
            if (os.path.exists(dir)):
                if (os.path.isdir(dir)):
                    continue
                else:
                    bad_paths.append(dir)
            else:
                if (os.path.exists(os.path.dirname(dir))):
                    os.mkdir(dir)
                else:
                    bad_paths.append(dir) 
    else:
        for dir in dirs:
            if (not os.path.exists(dir)) or (not os.path.isdir(dir)):
                bad_paths.append(dir)
    return bad_paths if len(bad_paths) else None


def build (frontend_root: str):
    command: list[str] = ['npm', "run", "build"]
    proc = subprocess.Popen(command, cwd=frontend_root, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    rcode: int = proc.wait()
    out: str = (proc.stdout.read()).decode("utf-8") 
    err: str = (proc.stderr.read()).decode("utf-8") 
    print(f"[Building return code]:\n{rcode}")
    print("[Building stdout]:\n", out)
    print("[Building stderr]:\n", err)


def deploy_content (
    front_index_fpath: str,
    front_favicon_fpath: str,
    front_js_dir: str,
    front_css_dir: str,
    front_img_dir: str,
    back_templates_dir: str,
    back_static_dir: str,
    delete_old = True
) -> None:
    check: Optional[list[str]] = check_files(front_index_fpath, front_favicon_fpath)
    if check is not None:
        raise Exception(f"Files {check} do not exist")
    check: Optional[list[str]] = check_directories(front_js_dir, front_css_dir, front_img_dir, create=False)
    if check is not None:
        raise Exception(f"Directories {check} do not exist")
    check: Optional[list[str]] = check_directories(back_templates_dir, back_static_dir, create=True)
    if check is not None:
        raise Exception(f"Directories {check} do not exist and can not be created")
    back_index_fpath: str = os.path.join(back_templates_dir, os.path.basename(os.path.normpath(front_index_fpath)))
    back_favicon_fpath: str = os.path.join(back_templates_dir, os.path.basename(os.path.normpath(front_favicon_fpath)))
    back_js_dirpath: str = os.path.join(back_static_dir, os.path.basename(os.path.normpath(front_js_dir)))
    back_css_dirpath: str = os.path.join(back_static_dir, os.path.basename(os.path.normpath(front_css_dir)))
    back_img_dirpath: str = os.path.join(back_static_dir, os.path.basename(os.path.normpath(front_img_dir)))
    if delete_old:
        if os.path.exists(back_index_fpath):
            os.remove(back_index_fpath)
        if os.path.exists(back_favicon_fpath):
            os.remove(back_favicon_fpath)
        shutil.rmtree(back_js_dirpath, ignore_errors=True)
        shutil.rmtree(back_css_dirpath, ignore_errors=True)
        shutil.rmtree(back_img_dirpath, ignore_errors=True)
    shutil.copyfile(front_index_fpath, back_index_fpath)
    shutil.copyfile(front_favicon_fpath, back_favicon_fpath)
    shutil.copytree(front_js_dir, back_js_dirpath)
    shutil.copytree(front_css_dir, back_css_dirpath)
    shutil.copytree(front_img_dir, back_img_dirpath)


def launch() -> None:
    config: dict = get_json_data(get_config_file(__default_config_file))
    config_front: dict = config["frontend"]
    config_back: dict = config["backend"]
    build(config_front["root"])
    deploy_content(
        os.path.join(config_front["build_dir"], config_front['index_subfpath']),
        os.path.join(config_front["build_dir"], config_front['favicon_subfpath']),
        os.path.join(config_front["build_dir"], config_front['js_subdir']),
        os.path.join(config_front["build_dir"], config_front['css_subdir']),
        os.path.join(config_front["build_dir"], config_front['img_subdir']),
        os.path.join(config_back["application_dir"], config_back['templates_subdir']),
        os.path.join(config_back["application_dir"], config_back['static_subdir'])
    )


if __name__ == "__main__":
    launch()
