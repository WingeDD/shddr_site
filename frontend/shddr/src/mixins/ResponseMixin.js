export default {
  methods: {
    fixPath (path) {
      let b = process.env.BASE_URL
      if (b !== undefined && b !== '' && b !== '/') {
        let p = (path.charAt(0) === '/') ? path : '/' + path
        b = (b.charAt(0) === '/') ? b : '/' + b
        if (p.indexOf(b) === 0) {
          p = p.replace(b, '')
          return (p.charAt(0) === '/') ? p : '/' + p
        } else {
          return p
        }
      } else {
        return path
      }
    },

    noExceptStrToJSON (jsonStr) {
      let data
      try {
        data = JSON.parse(jsonStr)
      } catch (e) {
        if (!(e instanceof SyntaxError)) {
          throw e
        }
      }
      return data
    },

    // checkFunc args: http_status: Integer, http_body_data: Object
    // checkFunc returns: null (self handled final state), undefined (no error, go next) or error message: Option[String]
    checkResponse (response, ...checkFuncs) {
      const status = response.status
      let data
      if (typeof response.data === 'string' || response.data instanceof String) {
        data = this.noExceptStrToJSON(response.data)
        if (data === undefined) {
          data = response.data
        }
      } else {
        data = response.data
      }
      for (const checkFunc of checkFuncs) {
        const errMsg = checkFunc(status, data)
        if (errMsg !== undefined) {
          return [status, data, errMsg]
        }
      }
    },

    fullErrMsg (status, data, errMsg) {
      return 'Error message: ' + errMsg + '\n' + 'Status code: ' + status.toString() + '\n' + 'Data: ' + JSON.stringify(data)
    },

    stdCheck302 (status, data) {
      if (status !== 302) {
        return undefined
      }
      if (!('redirection_route' in data)) {
        return 'Got status code 302 but "redirection_route" is absent'
      }
      this.$router.push(this.fixPath(data.redirection_route))
      return null
    },

    stdCheck400 (status, data) {
      if (status !== 400) {
        return undefined
      }
      return 'Bad request'
    },

    stdCheck401 (status, data) {
      if (status !== 401) {
        return undefined
      }
      if ('redirection_route' in data) {
        if ('forwarded_route' in data) {
          this.$store.commit('fwdRedirection/setForwardedRoute', data.forwarded_route)
        }
        this.$store.commit('loginChecking/setEmail', undefined)
        this.$router.push(this.fixPath(data.redirection_route))
        return null
      } else {
        return 'Authorization failed'
      }
    },

    stdCheck403 (status, data) {
      if (status !== 403) {
        return undefined
      }
      return 'Resource is forbidden'
    },

    stdCheck404 (status, data) {
      if (status !== 404) {
        return undefined
      }
      return 'Resource not found'
    },

    stdCheck500 (status, data) {
      if (status !== 500) {
        return undefined
      }
      return 'Internal server error'
    },

    stdCheckUnexpected (status, data) {
      return 'Unexpected status code: ' + status.toString() + '; JSON data: ' + JSON.stringify(data)
    }
  }
}
