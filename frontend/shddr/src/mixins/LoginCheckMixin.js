import ResponseMixin from '@/mixins/ResponseMixin'
export default {
  mixins: [ResponseMixin],
  methods: {
    async checkLoggedIn () {
      const response = await this.axios.request({
        method: 'POST',
        url: '/auth/check',
        validateStatus: () => true
      })
      const [status, data, errMsg] = this.checkResponse(
        response,
        (status, data) => {
          if (status === 200) {
            if ('email' in data) {
              this.$store.commit('loginChecking/setEmail', data.email)
              return null
            } else {
              return 'Got status code 200 on /auth/check but "email" is absent'
            }
          } else {
            return undefined
          }
        },
        this.stdCheck400,
        (status, data) => {
          if (status === 401) {
            this.$store.commit('loginChecking/setEmail', undefined)
            return null
          } else {
            return undefined
          }
        },
        this.stdCheck404,
        this.stdCheck500,
        this.stdCheckUnexpected
      )
      if ((errMsg !== undefined) && (errMsg !== null)) {
        this.$store.commit('errHandling/raiseError', this.fullErrMsg(status, data, errMsg))
      }
    }
  }
}
