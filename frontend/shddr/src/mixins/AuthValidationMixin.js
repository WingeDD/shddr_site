export default {
  data () {
    return {
      validEmailMaxLength: 128,
      validEmailRegex: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
      validPasswordMinLength: 8
    }
  },
  methods: {
    validateEmail (email) {
      return email.length <= this.validEmailMaxLength && email.match(this.validEmailRegex) ? undefined : 'Email must match valid email pattern and consist of no more than 128 characters'
    },
    validatePassword (password) {
      return password.length >= this.validPasswordMinLength ? undefined : 'Password must consist of at least ' + this.validPasswordMinLength.toString() + ' characters'
    },
    validatePasswordConfirmation (password, passwordConfirmation) {
      return password === passwordConfirmation ? undefined : 'Password does not match password confirmation'
    }
  }
}
