
export const errHandling = {
  state: () => ({
    error: undefined,
    visible: false
  }),
  getters: {
    getError (state) {
      return state.error
    },
    getVisible (state) {
      return state.visible
    }
  },
  mutations: {
    setError (state, error) {
      state.error = error
    },
    raiseError (state, error) {
      state.error = error
      state.visible = true
    },
    clean (state) {
      state.error = undefined
    },
    show (state) {
      state.visible = true
    },
    hide (state) {
      state.visible = false
    }
  },
  actions: {
  },
  namespaced: true
}
