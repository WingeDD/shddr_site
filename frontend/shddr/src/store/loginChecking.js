export const loginChecking = {
  state: () => ({
    email: undefined
  }),
  getters: {
    getEmail (state) {
      return state.email
    }
  },
  mutations: {
    setEmail (state, email) {
      state.email = email
    }
  },
  actions: {
  },
  namespaced: true
}
