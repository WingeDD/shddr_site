
export const fwdRedirection = {
  state: () => ({
    forwardedRoute: undefined
  }),
  getters: {
    getForwardedRoute (state) {
      return state.forwardedRoute
    }
  },
  mutations: {
    setForwardedRoute (state, forwardedRoute) {
      state.forwardedRoute = forwardedRoute
    }
  },
  actions: {
    consumeForwardedRoute ({ state, commit }) {
      const forwardedRoute = state.forwardedRoute
      commit('setForwardedRoute', undefined)
      return forwardedRoute
    }
  },
  namespaced: true
}
