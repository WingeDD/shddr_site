import { createStore } from 'vuex'
import { fwdRedirection } from '@/store/fwdRedirection'
import { errHandling } from '@/store/errHandling'
import { loginChecking } from '@/store/loginChecking'

export default createStore({
  state: {
    // just data
  },
  getters: {
    // getters; args: state, getters
  },
  mutations: {
    // for example setters; args: state, forwardedMethod
  },
  actions: {
    // more complex functions which don't mutate state themselves but can mutate it with mutations; args: context {...}
  },
  modules: {
    fwdRedirection: fwdRedirection,
    errHandling: errHandling,
    loginChecking: loginChecking
  }
})
